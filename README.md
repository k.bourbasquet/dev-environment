# Development environment with Docker-compose #

The purpose of this project is to provide a simple PHP development environment with a database.  
[Here](https://www.tablesgenerator.com/markdown_tables#) is the table generator I used for this README.

## How to use ##

### Requirements ###

- git
- docker
- docker-compose
- and the most important: *your brain*

### Step by step ###

Firstable, clone the repos with the command as follow :
```
git clone git@gitlab.com:k.bourbasquet/dev-environment.git
```
You can now go to the folder.
```
cd dev-environment
```
Then, execute the docker-compose file if you are satisfied by your configuration.  
*You can add the option -d to execute the server in background.*
```
docker-compose up
```
Now your website, PhpMyAdmin and PgAdmin are accessible on the ports you have defined.

## Environment ##

**Warning** *Warning I advise you to check if one of the ports below are not already used. Otherwise, it will create a conflict.  
You can change the ports to be opened directly in the docker-composer.
As a reminder, the public port is on the left and not on the right.*

| Name       	| Version 	| Open ports 	|
|------------	|---------	|------------	|
| Apache 2   	| latest  	| 80:80      	|
| Php        	| latest  	| no          	|
| MySQL      	| latest  	| 3306:3306  	|
| Postgres   	| latest  	| 5432:5432  	|
| PhpMyAdmin 	| latest  	| 8080:80    	|
| PgAdmin 4  	| latest  	| 8000:80    	|

## Identification ##

**Warning** *I advise you to change the identification values if you want to do something safer !*

| Name       	| User           	| Password 	|
|------------	|----------------	|----------	|
| MySQL      	| me             	| 1234     	|
| Postgres   	| me             	| 1234     	|
| PhpMyAdmin 	| me             	| 1234     	|
| PgAdmin 4  	| 1234@admin.com 	| 1234     	|

## Docker-compose ##

```
version: "3.4"

volumes:
  postgres_data:
  pgadmin_data:
  mysql_data:
  pma_data:

services:
  postgres:
    image: postgres:alpine
    ports:
      - "5432:5432"
    environment:
      POSTGRES_USER: me
      POSTGRES_PASSWORD: 1234
      POSTGRES_DB: dev
    volumes:
      - postgres_data:/var/lib/postgresql/data/

  pgAdmin:
    restart: always
    image: dpage/pgadmin4
    ports:
      - "8000:80"
    environment:
      PGADMIN_DEFAULT_EMAIL: 1234@admin.com
      PGADMIN_DEFAULT_PASSWORD: 1234
    volumes:
      - pgadmin_data:/var/lib/pgadmin
    depends_on:
      - postgres

  mysql:
    image: mysql
    environment:
      MYSQL_ROOT_PASSWORD: 1234
      MYSQL_USER: me
      MYSQL_PASSWORD: 1234
    ports:
      - "3306:3306"
    volumes:
      - mysql_data:/var/lib/mysql/

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_ARBITRARY: 1
      PMA_HOST: mysql
      PMA_PORT: 3306
      PMA_USER: me
      PMA_PASSWORD: 1234
    restart: always
    ports:
      - "8080:80"
    volumes:
      - pma_data:/sessions
    depends_on:
      - mysql

  apache:
    build:
      context: ./
      dockerfile: Dockerfile-apache
    volumes:
      - ./html:/var/www/html/
    ports:
      - "80:80"
    depends_on:
      - postgres
      - mysql
```

## Work directory path ##

You can change the path to your work directory by changing the ./html to the path you want in the volumes section of the apache service.

## Coming soon ##

- JSON file to connect pgadmin to postrgesql with docker-compose
- .env file

## Credit ##

Created by @k.bourbasquet - Review of the README by @lilian-pouliquen